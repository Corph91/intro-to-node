var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));

app.get('/get',function(req,res){
    res.send("Hallo!");
});

app.get('/', function(req,res){
    res.send("Home!!!!");
});

app.get('/happy', function(req,res){
    res.send("I won the game!");
});

app.get('/sad', function(req,res){
    res.send("I lost the game!");
});

app.post('/login',function(req,res){
    //req.body is the form
    var name = req.body.name;
    var password = req.body.password;
    var user = {
        name: name,
        password: password
    }

    res.json(user);
});

var server = app.listen(3001, function(){
    console.log("Listening on: 3001");
});